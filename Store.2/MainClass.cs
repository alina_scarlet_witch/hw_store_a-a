﻿using System;
namespace store._2
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Shop shop = new Shop();
            Statistic statistic = new Statistic();
            Menu.StartMessage();       
            do
            {
                char choise = Menu.ShowActionInStore();
                switch (choise)
                {
                    case '1':
                        shop.ShowShelfes();
                        shop.ShowBuyers();
                        shop.Check();
                        Console.ReadKey();
                        break;
                    case '2':                      
                        statistic.ShowProductsAtTheStorage();
                        break;
                    case '3':
                        return;
                    default:
                        Console.WriteLine("You have been entered wrong data.");
                        break;
                }
            } while (true);
        }
    }
}
