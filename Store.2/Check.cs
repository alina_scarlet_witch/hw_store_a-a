﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace store._2
{
    public class Check
    {
        public List<ListOfBuys> ListOfBuys = new List<ListOfBuys>();
        public int IDCheck;
        public string NameOfCustomer;
        public int IDofBuyer;
        public double SumOfCheck;
        public string CheckIsPayed;
        public DateTime DateOfCheck;
       
        public Check(int idCheck, string nameOfCustomer, int iDofBuyer , DateTime dateOfCheck)
        {
            NameOfCustomer = nameOfCustomer;
            IDofBuyer = iDofBuyer;
            DateOfCheck = dateOfCheck;
            IDCheck = idCheck;
        }
    }
}
