﻿using System;


namespace store._2
{
    public class Product
    {
        public int ID;
        public string ProductName;
        public DateTime ManufactureDataProduct;
        public DateTime EndDateProduct;
        public string Category;
        public double Price;
        public double Count; 
        public int ShelfLife; 
        public int Time; 
        public double SizeProduct;           
        public Product(int id, string nameOfProduct, string category, double price,
                           double count, int shelfLife, double sizeProduct, DateTime time)
        {
            ID = id;
            ProductName = nameOfProduct;
            Price = price;
            Count = count;
            ShelfLife = shelfLife;
            Category = category;
            SizeProduct = sizeProduct;
            ManufactureDataProduct = time;
            Time = ShelfLife * 30;
            EndDateProduct = time.AddSeconds(Time);
        }         
    }
}
