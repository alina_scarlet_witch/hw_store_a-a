﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace store._2
{
    public class Buyer
    {
        public int ID;
        public string NameBuyer;
        public double Vollet;
        public List<ListOfWishes> GoodsList = new List<ListOfWishes>();
        public Buyer()
        {
        }
        public Buyer(int id, string nameBuyer, double vollet)
        {
            ID = id;
            NameBuyer = nameBuyer;
            Vollet = vollet;
        }
    }
}
